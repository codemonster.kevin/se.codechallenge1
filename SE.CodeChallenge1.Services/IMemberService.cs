﻿using SE.CodeChallenge1.Model;
using System;
using System.Collections.Generic;

namespace SE.CodeChallenge1.Services
{
    public interface IMemberService
    {
        /// <summary>
        /// returns the Member object by matching Id
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns></returns>
        Member GetMemberById(Guid memberId);

        /// <summary>
        /// Finds the member that contains the search text
        /// </summary>
        /// <param name="searchText"></param>
        /// <returns></returns>
        List<Member> FindMemberByName(string searchText);

        /// <summary>
        /// Creates a new Member and saves to database
        /// Rejects or returns an error if email already exists
        /// Returns the new Member object if successful
        /// </summary>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="contactNo"></param>
        /// <param name="accountStatus"></param>
        /// <returns></returns>
        Member CreateNewMember(string name, string email, string contactNo, int accountStatus);

        /// <summary>
        /// Updates the members' info by matching Id
        /// If the email has changed, rejects or returns an error if the new email already exists
        /// Returns the new member object with the updated details
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="name"></param>
        /// <param name="contactNo"></param>
        /// <param name="accountStatus"></param>
        /// <returns></returns>
        Member UpdateMember(Guid id, string name, string email, string contactNo, int accountStatus);
    }
}
