﻿namespace SE.CodeChallenge1.Model
{
    public class Member : Entity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public int AccountStatus { get; set; }
    }
}
