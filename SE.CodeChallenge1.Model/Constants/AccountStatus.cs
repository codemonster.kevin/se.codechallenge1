﻿namespace SE.CodeChallenge1.Model
{
    public enum AccountStatus
    {
        TRIAL = 1,
        BASIC = 2,
        PROFESSIONAL = 3,
        ENTERPRISE = 4
    }
}
